package com.example.demo.controller;

import com.example.demo.pojo.domain.UserDO;
import com.example.demo.service.UserService;
import com.github.pagehelper.PageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@RestController
public class HelloController {
    @Resource
    private UserService userService;

    @GetMapping("/hello")
    public Object hello() {
        // 【改】返回类型为Mono<String>
        // 【改】使用Mono.just生成响应式数据
        List<UserDO> userDO = userService.listUserDOS();
        if (Objects.nonNull(userDO)) {
            return userDO;
        }
        return Mono.just("Welcome to reactive world ~");
    }
}
