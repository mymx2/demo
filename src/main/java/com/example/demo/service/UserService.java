package com.example.demo.service;

import com.example.demo.pojo.domain.UserDO;

import java.util.List;

/**
 * @author DYUN
 */
public interface UserService {
    /**
     * 查询Demo
     * @return list
     */
    List<UserDO> listUserDOS();
}
