package com.example.demo.service.impl;

import com.example.demo.mapper.UserMapper;
import com.example.demo.pojo.domain.UserDO;
import com.example.demo.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author DYUN
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public List<UserDO> listUserDOS() {
        PageHelper.startPage(1, 1);
        List<UserDO> userDOList = userMapper.listUser();
        PageInfo<UserDO> pageInfo = new PageInfo<>(userDOList);
        log.debug("sss" + pageInfo);
        return userDOList;
    }
}
