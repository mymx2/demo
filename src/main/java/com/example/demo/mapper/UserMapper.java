package com.example.demo.mapper;

import com.example.demo.pojo.domain.UserDO;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface UserMapper extends Mapper<UserDO> {
    @Select("select * from sys_user")
    List<UserDO> listUser();
}
