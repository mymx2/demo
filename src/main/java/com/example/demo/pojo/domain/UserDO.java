package com.example.demo.pojo.domain;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author DYUN
 */
@Table(name = "sys_user")
@Data
public class UserDO implements Serializable {
    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;
}
